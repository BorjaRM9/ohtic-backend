package com.ohtic.test.productsmanagement.common.api;

public interface Product {
	
	public Long getProductId();

	public void setProductId(Long productId);

	public String getName();

	public void setName(String name);

	public String getDescription();

	public void setDescription(String description);

	public String getImage();

	public void setImage(String image);

	public String getSeller();

	public void setSeller(String seller);

	public Integer getPrice();

	public void setPrice(Integer price);

	public Integer getQuantity();

	public void setQuantity(Integer quantity);

}
