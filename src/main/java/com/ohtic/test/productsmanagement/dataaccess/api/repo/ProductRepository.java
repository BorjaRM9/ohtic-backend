package com.ohtic.test.productsmanagement.dataaccess.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ohtic.test.productsmanagement.dataaccess.api.ProductEntity;


public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

}
