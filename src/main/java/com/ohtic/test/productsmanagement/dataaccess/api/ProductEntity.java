package com.ohtic.test.productsmanagement.dataaccess.api;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ohtic.test.productsmanagement.common.api.Product;

@Entity
@Table(name = "Product")
public class ProductEntity implements Product, Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long productId;
	
	private String name;
	
	private String description;
	
	private String image;
	
	private String seller;
	
	private Integer price;
	
	private Integer quantity;
	
	private static final long serialVersionUID = 1L;

	@Override
	public Long getProductId() {
		return productId;
	}

	@Override
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getImage() {
		return image;
	}

	@Override
	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String getSeller() {
		return seller;
	}

	@Override
	public void setSeller(String seller) {
		this.seller = seller;
	}

	@Override
	public Integer getPrice() {
		return price;
	}

	@Override
	public void setPrice(Integer price) {
		this.price = price;
	}

	@Override
	public Integer getQuantity() {
		return quantity;
	}

	@Override
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
