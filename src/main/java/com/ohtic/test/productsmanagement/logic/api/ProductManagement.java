package com.ohtic.test.productsmanagement.logic.api;

import java.util.List;

import com.ohtic.test.productsmanagement.logic.api.to.ProductDto;

public interface ProductManagement {
		
	public List<ProductDto> getProducts();

}
