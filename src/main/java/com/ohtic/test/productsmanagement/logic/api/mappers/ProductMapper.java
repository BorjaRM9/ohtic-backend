package com.ohtic.test.productsmanagement.logic.api.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ohtic.test.productsmanagement.dataaccess.api.ProductEntity;
import com.ohtic.test.productsmanagement.logic.api.to.ProductDto;

@Mapper
public interface ProductMapper {

	ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

	ProductDto productEntityToProductDto(ProductEntity product);

	ProductEntity productDtoToProductEntity(ProductDto product);

	List<ProductDto> productDtoListToProductEntityList(List<ProductEntity> products);
	
	List<ProductEntity> productEntityListToProductDtoList(List<ProductDto> products);

}
