package com.ohtic.test.productsmanagement.logic.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.ohtic.test.productsmanagement.dataaccess.api.ProductEntity;
import com.ohtic.test.productsmanagement.dataaccess.api.repo.ProductRepository;
import com.ohtic.test.productsmanagement.logic.api.ProductManagement;
import com.ohtic.test.productsmanagement.logic.api.mappers.ProductMapper;
import com.ohtic.test.productsmanagement.logic.api.to.ProductDto;

@Named
@Validated
@Transactional
public class ProductManagementImpl implements ProductManagement {
	
	@Inject
	private ProductRepository productRepository;

	@Override
	public List<ProductDto> getProducts() {
		
		List<ProductEntity> products = this.productRepository.findAll();
						
		List<ProductDto> etos = ProductMapper.INSTANCE.productDtoListToProductEntityList(products);
		
		return etos;
	}

}
