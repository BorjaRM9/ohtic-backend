package com.ohtic.test.productsmanagement.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ohtic.test.productsmanagement.logic.api.ProductManagement;
import com.ohtic.test.productsmanagement.logic.api.to.ProductDto;
import com.ohtic.test.productsmanagement.service.api.rest.ProductManagementRestService;

@RestController
@RequestMapping("productmanagement/v1")
public class ProductManagementRestServiceImpl implements ProductManagementRestService {
	
	@Inject
	private ProductManagement productManagement;
	
	@Override
	@GetMapping("/products")
	public List<ProductDto> getProducts() {
		return this.productManagement.getProducts();
	}

}
