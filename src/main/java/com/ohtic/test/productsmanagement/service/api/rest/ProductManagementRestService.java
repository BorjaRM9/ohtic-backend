package com.ohtic.test.productsmanagement.service.api.rest;

import java.util.List;

import com.ohtic.test.productsmanagement.logic.api.to.ProductDto;

public interface ProductManagementRestService {

	public List<ProductDto> getProducts();
	
}
