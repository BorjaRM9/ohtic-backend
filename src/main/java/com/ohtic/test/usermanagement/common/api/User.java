package com.ohtic.test.usermanagement.common.api;

public interface User {
	
	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);
	
}
