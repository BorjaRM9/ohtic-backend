package com.ohtic.test.usermanagement.service.api.rest;

import com.ohtic.test.usermanagement.logic.api.to.UserDto;

public interface UserManagementRestService {

	public void registerUser(UserDto user);
	
}
