package com.ohtic.test.usermanagement.service.impl;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ohtic.test.usermanagement.logic.api.UserManagement;
import com.ohtic.test.usermanagement.logic.api.to.UserDto;
import com.ohtic.test.usermanagement.service.api.rest.UserManagementRestService;

@RestController
@RequestMapping("usermanagement/v1")
public class UserManagementRestServiceImpl implements UserManagementRestService {

	@Inject
	private UserManagement userManagement;

	@Override
	@PostMapping(value = "/register", consumes = "application/json")
	public void registerUser(@RequestBody UserDto user) {
		this.userManagement.registerUser(user);
	}

}
