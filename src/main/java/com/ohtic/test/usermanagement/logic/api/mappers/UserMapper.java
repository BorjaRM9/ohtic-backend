package com.ohtic.test.usermanagement.logic.api.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ohtic.test.usermanagement.dataaccess.api.UserEntity;
import com.ohtic.test.usermanagement.logic.api.to.UserDto;

@Mapper
public interface UserMapper {
	
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

	UserDto userEntityToUserDto(UserEntity user);

	UserEntity userDtoToUserEntity(UserDto user);

}
