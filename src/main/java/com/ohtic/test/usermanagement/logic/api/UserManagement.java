package com.ohtic.test.usermanagement.logic.api;

import com.ohtic.test.usermanagement.logic.api.to.UserDto;

public interface UserManagement {

	public void registerUser(UserDto user);
	
}
