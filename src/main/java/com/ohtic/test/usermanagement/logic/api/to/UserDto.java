package com.ohtic.test.usermanagement.logic.api.to;

import java.io.Serializable;

import com.ohtic.test.usermanagement.common.api.User;

public class UserDto implements User, Serializable {
	
	private String username;
	
	private String password;
		
	private static final long serialVersionUID = 1L;

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}
	
}
