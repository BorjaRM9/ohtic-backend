package com.ohtic.test.usermanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.ohtic.test.usermanagement.dataaccess.api.UserEntity;
import com.ohtic.test.usermanagement.dataaccess.api.repo.UserRepository;
import com.ohtic.test.usermanagement.logic.api.UserManagement;
import com.ohtic.test.usermanagement.logic.api.mappers.UserMapper;
import com.ohtic.test.usermanagement.logic.api.to.UserDto;

@Named
@Validated
@Transactional
public class UserManagementImpl implements UserManagement {
	@Inject
	private UserRepository userRepository;

	@Inject
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void registerUser(UserDto user) {

		UserEntity userEntity = UserMapper.INSTANCE.userDtoToUserEntity(user);

		userEntity.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

		this.userRepository.save(userEntity);
	}

}
