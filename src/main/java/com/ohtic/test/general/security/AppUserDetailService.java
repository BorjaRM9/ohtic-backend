package com.ohtic.test.general.security;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ohtic.test.usermanagement.dataaccess.api.UserEntity;
import com.ohtic.test.usermanagement.dataaccess.api.repo.UserRepository;

@Service
public class AppUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    	Optional<UserEntity> userEntity = this.userRepository.findByUsername(username);
    	
    	if (userEntity.isEmpty()) {
			throw new UsernameNotFoundException("User '" + username + "' not found");
		}
    	
    	UserDetails userDetails = 
    			org.springframework.security.core.userdetails.User
                .withUsername(username)
                .password(userEntity.get().getPassword())
                .authorities(Collections.emptyList())
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    	    	
        return userDetails;
        
    }

}
