INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (1, 'Pig pillow', 'Almohada de felpa imitación peluche de cerdo', '/assets/images/products/p02.jpg', 'Aliexpress', 20, 15);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (2, 'Cocoon pillow', 'Perfecta para hacer el ridículo en cualquier sitio!', '/assets/images/products/p03.jpg', 'Ostrichpillow', 90, 161);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (3, 'Oppo dog muzzle quack', 'description', '/assets/images/products/p01.jpg', 'Japantrendshop', 32, 7);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (4, 'Camiseta solar', 'When the sun is out that now means a fully charged phone.', '/assets/images/products/p04.jpg', 'unnecessaryinventions', 16, 1);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (5, 'Lazy glasses', 'High definition periscope glasses that allow you to read horizontally, or even watch TV while lying down.', '/assets/images/products/p13.jpg', 'Amazon', 8, 17);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (6, 'Pecera portable', 'Makes your goldfish happy, or totally pointless?', '/assets/images/products/p08.jpg', 'Aliexpress', 12, 15);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (7, 'Doggie dryer', 'Good luck getting your dog inside this hilarious doggie dryer', '/assets/images/products/p07.jpg', 'Amazon', 30, 4);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (8, 'Perstañas LED', 'Perfect for rave parties and if you feel like going blind by questionable Chinese tech!', '/assets/images/products/p15.jpg', 'Aliexpress', 2, 2);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (9, 'Archie McPhee ardilla', 'description', '/assets/images/products/p09.jpg', 'Amazon', 8, 40);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (10, 'Chanclas grass', 'description', '/assets/images/products/p05.jpg', 'Amazon', 19, 15);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (11, 'Absolutamente NADA', 'Para el que lo tiene todo', '/assets/images/products/p17.jpg', 'Amazon', 10, 40);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (12, 'Handerpants', 'Ropa interior para tus manos', '/assets/images/products/p14.png', 'Amazon', 12, 40);

INSERT INTO Product (product_Id, name, description, image, seller, price, quantity) 
VALUES (13, 'Porta hamburguesas', 'descripcion', '/assets/images/products/p16.jpg', 'Amazon', 17, 40);

INSERT INTO User (id, username, password) VALUES (1, 'user', '$2a$12$zQgQGJBEILf9E3.Z0NBXGOKDOEzok4qNk9rNUpxoeVCEQ4IpjjxFG');


